
import java.util.Scanner;
// import java.util.Random;

public void ComCajero(){
		ATM CajeroAutomatico = new ATM();
		CajeroAutomatico.ComCajero();
	}


class ATM {
	public void ComCajero(){
	Cuenta[] arregloDeCuentas = new Cuenta[10]; // hace el arreglo de las diez cuentas que deben ser creadas
	for(int i = 0; i < arregloDeCuentas.length; i++) {

		arregloDeCuentas[i] = new Cuenta(i,100000); // BALANCE INICIAL DE 100,000 CRC
  }
  while (true){ // para que no se salga del programa
  
  System.out.println("Por favor digite su id: \n");
  Scanner lector = new Scanner(System.in);
  int idDigitado = lector.nextInt();

// SE DESPLIEGA EL MENÚ UNA VEZ ACEPTADO EL ID 
  while (idDigitado<10 && idDigitado>=0) {
  
            System.out.println("====MENÚ PRINCIPAL====\n");
			System.out.println("Ingrese su selección");
			System.out.println("1. Ver el balance actual");
			System.out.println("2. Retirar dinero");
			System.out.println("3. Depositar dinero");
			System.out.println("4. Salir");
			
    int seleccion = lector.nextInt();
  
  // SE PRUEBAN TODOS LOS POSIBLES CASOS EN LA OPCIONES 
  
  switch (seleccion){
  
    case 1:
       System.out.println("El balance actual es de: \n"+arregloDeCuentas[idDigitado].getBalance());
       break;
       
    case 2:

            System.out.println("Digite el monto que desea retirar:\n");
			double dineroARetirar = lector.nextDouble();
			arregloDeCuentas[idDigitado].retirarDinero(dineroARetirar);
			break;
    
    case 3:
					System.out.println("Digite el monto que desea depositar:\n");
					double dineroADepositar = lector.nextDouble();
					arregloDeCuentas[idDigitado].depositarDinero(dineroADepositar);
					break;
    
    case 4:
      System.out.println("Saliendo del programa...");
      break;
      
      
// EN CASO DE NO PONER UN ID VALIDO SE CIERRA EL PORGRAMA.




  }

            }
        }
	}
}