import java.util.Date;

// Crea la clase Cuenta, y define los atributos a,b,c,d 

public class Cuenta{
  private int id;
  private double balance;
  private double tasaDeInteresAnual;
  private Date fechaDeCreacion= new Date();

// crea los valores por defecto

  Cuenta(){
    this.id = 0;
    this.balance = 0;
    this.tasaDeInteresAnual = 0;
    this.fechaDeCreacion = new Date();

  }

//constructor sin argumentos crea las cuentas por defecto e

  public Cuenta (int id, double balance) {
    this.id = id;
    this.balance = balance;
  }

// get y set para id, balance, tasaDeInteresAnual g

// ID

  public int getId() {
    return this.id;
    }

    public void setId(int id){
     this.id = id;
  }

// BALANCE

  public double getBalance() {
    return this.balance;
    }

    public void setBalance(double balance){
     this.balance = balance;
  }
  
// TASADEINTERESANUAL

  public double getTasaDeInteresAnual() {
    return this.tasaDeInteresAnual;
    }

    public void setTasaDeInteresAnual(int tasaDeInteresAnual){
     this.tasaDeInteresAnual = tasaDeInteresAnual;
  }

// GET PARA ACCEDER A LA FECHA DE CREACIÓN h
public Date getFecha() {
		return this.fechaDeCreacion;
		}

// MÉTODO PARA OBTENER LA TASA DE INTERES MENSUAL i
public double obtenerTasaDeInteresMensual(double tasaDeInteresAnual) {
			double InteresMensual = tasaDeInteresAnual /12;
			return InteresMensual;
}

// METODO PARA MULTIPLICAR BALANCE Y TASA DE INTERES ANUAL j

public double calcularInteresMensual (double balance, double tasaDeInteresAnual){
  double InteresMensual = balance*tasaDeInteresAnual;
  return InteresMensual;
}

//MÉTODO PARA RETIRAR DINERO 

public void retirarDinero(double dineroARetirar ){
  balance= balance-dineroARetirar;
}

public void depositarDinero (double montoDeDinero){
  balance= balance+montoDeDinero;
}


  }