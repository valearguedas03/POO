// Enlace de repl https://repl.it/@ValeArguedas/Lab-1-poo
// Raychell Arguedas 2019027258

import java.util.Date;

class Main {
  public static void main(String[] args) { 

// primer cliente con id y balance. cliente1 agarra los atributos de Cuenta

    Cuenta cliente1= new Cuenta(1122,500000);  
    
    cliente1.setTasaDeInteresAnual(0.045);
    cliente1.depositarDinero(150000);
    cliente1.retirarDinero(200000);
    System.out.println( "\nBalance del cliente 1: \n"
                        +cliente1.getBalance()
                        +"\nLa Tasa de Interes Mensual es:\n"
                        +cliente1.obtenerTasaDeInteresMensual()
                        +"\nFecha de creacion: " 
                        +cliente1.getFecha());
    
// primer cliente con id y balance. cliente2 agarra los atributos de Cuenta

    Cuenta cliente2 = new Cuenta(4023,40000);
    
    cliente2.setTasaDeInteresAnual(0.045);
    cliente2.depositarDinero(200000);
    cliente2.retirarDinero(100000);
    System.out.println( "\nBalance del cliente 1: \n"
                        +cliente2.getBalance()
                        +"\nLa Tasa de Interes Mensual es:\n"
                        +cliente2.obtenerTasaDeInteresMensual()
                        +"\nFecha de creacion: " 
                        +cliente2.getFecha());

  }
  
}
// Este programa tiene referencia al código de maquinas visto en clases. 
// Refuerzo con el blog donde tengo los resuemenes para desarrollar mejor el programa.