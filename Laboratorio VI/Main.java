// Raychell Arguedas Bolivar 
//2019027258

public class Main {
  public static void main(String[] args) {
    Object[] objects = {
      new AC(), new Estufa(), new Lavadora(),new Luces(), new Microondas(), new Refrigerador(), new TV()}; 
      for (int i = 0; i < objects.length; i++) {
        if (objects[i] instanceof Controlador) System.out.println(((Controlador)objects[i]).Condicion());
        if (objects[i] instanceof ReguladorTemperatura) { 
          System.out.println(((ReguladorTemperatura)objects[i]).Temperatura());
        }
      }
    }
}   

